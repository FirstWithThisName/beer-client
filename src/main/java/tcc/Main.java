/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc;

import tcc.api.BeerAPI;
import tcc.entities.Beer;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import tcc.api.BeerOfflineApi;
import tcc.api.BeerOnlineApi;

/**
 *
 * @author christian
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // options with there default values
        @SuppressWarnings("UnusedAssignment")
        boolean searchOnline = true;
        @SuppressWarnings("UnusedAssignment")
        boolean trySync = true;
        @SuppressWarnings("UnusedAssignment")
        String nameToLookFor = "";
        @SuppressWarnings("UnusedAssignment")
        int showBeersAtOnce = 3;

        try {

            Options options = new Options();
            Option name = new Option("n", true, "name to look for");
            name.setRequired(true);
            options.addOption(name);

            Option sync = new Option("ns", false, "Don't synchronise the local data with the data form the server (default true)");
            options.addOption(sync);

            Option offlineSearch = new Option("of", false, "offline search (default is online search)");
            options.addOption(offlineSearch);

            Option beersPerPage = new Option("b", true, "beer per page, number of beers shown at ones.");
            beersPerPage.setRequired(true);
            options.addOption(beersPerPage);

            CommandLineParser parser = new BasicParser();
            CommandLine cmd = parser.parse(options, args);

            nameToLookFor = cmd.getOptionValue("n");
            trySync = !cmd.hasOption("ns");
            showBeersAtOnce = Integer.parseInt(cmd.getOptionValue("b"));
            searchOnline = !cmd.hasOption("of");

        } catch (ParseException | NumberFormatException ex) {
            System.err.println("failed to parse command line args!");
            System.exit(0);
        }

        final Scanner scan = new Scanner(System.in);

        if (trySync && searchOnline) {
            trySync();
        }

        try {
            BeerAPI beerApi = searchOnline ? new BeerOnlineApi() : new BeerOfflineApi();

            int page = 1;
            List<Beer> beerPage;
            do {
                beerPage = beerApi.findBeersByName(nameToLookFor, showBeersAtOnce, page);
                BeerCmdUtil.presentBeerList(beerPage);
                page++;

                if (!beerPage.isEmpty()) {
                    System.out.println("string with y for next Page something else to end");
                } else {
                    break;
                }
            } while (scan.nextLine().contains("y"));
        } catch (Exception ex) {
            System.err.println("beer search failed");
            //ex.printStackTrace();
        } finally {
            scan.close();
        }

    }

    public static void trySync() {
        BeerPersitanceManager bpm = new BeerPersitanceManager();
        System.out.println("try to sync local beers ...");
        try {
            bpm.sync();
            System.out.println("sync done");
        } catch (IOException ex) {
            System.err.println("Sync failed");
//            bcu.showInfo(ex);
        }
    }
}
