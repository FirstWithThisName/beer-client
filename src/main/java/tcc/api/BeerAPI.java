/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.api;

import java.util.List;
import tcc.entities.Beer;

/**
 * Contains all functions for the BeerAPI.
 *
 * @author christian
 */
public interface BeerAPI {

    /**
     *
     * @param needle String to look for.
     * @param pageSize must be > 0 and less than 80.
     * @param page must be > 0.
     * @return founden beers.
     * @throws java.lang.Exception if an error occurs.
     */
    public List<Beer> findBeersByName(final String needle, final int pageSize, final int page) throws Exception;

}
