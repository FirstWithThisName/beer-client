/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.api;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.codehaus.jackson.map.ObjectMapper;
import tcc.BeerPersitanceManager;
import tcc.entities.Beer;

/**
 * The offline implementation of the BeerAPI.
 *
 * @author christian
 */
public class BeerOfflineApi implements BeerAPI {

    private final ObjectMapper mapper = new ObjectMapper();

    private static boolean nameMatches(String name, String name2) {
        return name.toLowerCase().contains(name2.toLowerCase());
    }

    @Override
    public List<Beer> findBeersByName(String needle, final int pageSize, final int page) throws IOException, JsonParseException {

        // https://stackoverflow.com/questions/9390368/java-best-approach-to-parse-huge-extra-large-json-file
        JsonFactory f = new MappingJsonFactory();
        List<Beer> re = new LinkedList<>();
        try (JsonParser jp = f.createJsonParser(BeerPersitanceManager.PERSISTANCE_FILE)) {
            JsonToken current;
            current = jp.nextToken();
            if (current != JsonToken.START_ARRAY) {
                System.out.println("Error: root should be object: quiting.");
                return null;
            }
            int found = 0;
            while (jp.nextToken() != JsonToken.END_ARRAY) {
                Beer read = jp.readValueAs(Beer.class);

                if (nameMatches(read.getName(), needle)) {
                    found++;

                    if (found > ((page - 1) * pageSize)) {
                        re.add(read);

                        if (re.size() >= pageSize) {
                            break;
                        }
                    }
                }
            }
        }

        return re;
    }

}
