package tcc.api;

import tcc.entities.Beer;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

/**
 * This is the online implementaion of the BeerAPI. Find the documentation for
 * the used REST-api
 * <a href="https://www.baeldung.com/java-http-request">here</a>.
 *
 * @see BeerOnlineConnetor
 * 
 * @author christian
 */
public class BeerOnlineApi implements BeerAPI {

    private final ObjectMapper mapper = new ObjectMapper();
    private final BeerOnlineConnetor b = new BeerOnlineConnetor();

    /**
     *
     * @param needle String to look for.
     * @param pageSize must be > 0
     * @param page must be > 0
     * @return founden beers.
     * @throws java.net.ProtocolException
     * @throws java.net.MalformedURLException
     */
    @Override
    public List<Beer> findBeersByName(String needle, final int pageSize, final int page) throws ProtocolException, MalformedURLException, IOException {

        // TODO escape special charaters in the name
        String strUrl = b.createStringURL(pageSize, page) + "&beer_name=" + needle.replaceAll(" ", "_");
        URL url = new URL(strUrl);
        HttpsURLConnection con = b.getConnection(url);

        List<Beer> readValue;

        try (InputStream in = con.getInputStream()) {

            readValue = mapper.readValue(in, new TypeReference<List<Beer>>() {
            });
        }
        con.disconnect();

        return readValue;
    }

}
