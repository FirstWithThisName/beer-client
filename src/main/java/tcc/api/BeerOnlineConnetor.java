/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.api;

import java.io.IOException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

/**
 * Helper Class containing information to connect to the Beer REST-API. Find the
 * documentation for the used REST-api
 * <a href="https://www.baeldung.com/java-http-request">here</a>.
 *
 * @author christian
 */
public class BeerOnlineConnetor {

    private static final String HOST_URL = "https://api.punkapi.com/v2/beers";

    public String createStringURL(final int pageSize, final int page) {
        return HOST_URL + "?per_page=" + Integer.toString(pageSize) + "&page=" + Integer.toString(page);
    }

    public HttpsURLConnection getConnection(final URL url) throws IOException {
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

        con.setRequestMethod("GET");
        con.setDoInput(true);
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        con.setConnectTimeout(5000); // 5 seconds
        con.setReadTimeout(5000); // 5 seconds

        return con;
    }
}
