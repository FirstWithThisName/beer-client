package tcc.entities;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author christian
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Beer implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;
    private String name;
    private String description;
    private BeerRecipe ingredients;

}
