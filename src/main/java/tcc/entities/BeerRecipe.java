/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc.entities;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author christian
 */
@Getter
@Setter
public class BeerRecipe implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Ingredient> malt = new LinkedList<>();
    private List<Ingredient> hops = new LinkedList<>();
    private String yeast;

}
